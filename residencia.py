from flask import Flask, jsonify, request
import messages_controller

app = Flask(__name__)

@app.route('/msg', methods=["GET"])
def get_msgs():
    mensajes = messages_controller.get_msg()
    return jsonify(mensajes)

@app.route("/msg", methods=["POST"])
def insert_msg():
    msg_details = request.get_json()
    content = msg_details["msg"]
    result = messages_controller.insert_msg(content)
    print("mensaje recibido")
    return "mensaje recibido"


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=False)
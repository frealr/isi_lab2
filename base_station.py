from flask import Flask, jsonify, request
import messages_controller
from db import create_tables
import serial
import serial.tools.list_ports as list_ports
import requests
from threading import Thread
import time  

PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 10


def find_comport(pid,vid,baud):
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid,p.vid))
        except AttributeError:
            continue
    if (p.pid == pid) and (p.vid == vid):
        print('found target device pid: {} vid: {} port: {}'.format(
            p.pid, p.vid, p.device))
        ser_port.port = str(p.device)
        return ser_port
    return None

app = Flask(__name__)


@app.route("/msg", methods=["POST"])
def insert_msg():

    msg_details = request.get_json()
    content = msg_details["content"]
    contentBytes = content.encode()
    ser_micro.write(contentBytes)
    return "mensaje: " + content + " enviado correctamente al microbit"


def task(period, message):
    while True:
        time.sleep(0.1)
        line = ser_micro.readline().decode('utf-8')
        if line:
            print("mensaje recibido: " + line)
            resp = requests.post('http://127.0.0.1:8000/msg',json = {'msg' : line})



if __name__ == "__main__":
    create_tables()
    print('looking for microbit')
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 9600)
    if not ser_micro:
        print('microbit not found')
    print('opening and monitoring microbit port')
    ser_micro.open()
    t = Thread(target=task, args=[1, "task running"])
    t.start()
    app.run(host='0.0.0.0', port=5000, debug=False)
    ser_micro.close()
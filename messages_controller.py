from db import get_db
def insert_msg(name):
    db = get_db()
    cursor = db.cursor()
    statement = "INSERT INTO mensajes(name) VALUES (?)"
    cursor.execute(statement, [name])
    db.commit()
    return True

def get_msg():
    db = get_db()
    cursor = db.cursor()
    query = "SELECT id, name FROM mensajes"
    cursor.execute(query)
    return cursor.fetchall()